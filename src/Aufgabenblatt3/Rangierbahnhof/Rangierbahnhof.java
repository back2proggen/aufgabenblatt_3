
/**
 * Praktikum TIPR2 ,WS 2015
 * Gruppe: Soufian Ben afia
 * 		   Felix Naumann
 * Aufgabenblatt 3, Aufgabe 1
 */

package Aufgabenblatt3.Rangierbahnhof;

/**
 * Die Klasse repr�sentiert ein Rangierbahnhof mit einer bestimmten Anzahl von
 * Gleisen.
 */

public class Rangierbahnhof {

	/**
	 * Die Anzahl der Gleise und in jedem Gleis kann genau ein Zug stehen
	 */
	private Zug[] gleise = new Zug[5];

	/**
	 * F�hrt ein Zug in ein Gleis rein falls der Gleis nicht besetzt ist
	 * 
	 * @param gleis
	 *            Die Gleisnummer
	 * @param zug
	 *            Zug welches eingefahren wird
	 */

	public synchronized void einfahren(int gleis, Zug zug) {

		if (gleise[gleis] == null) {
			gleise[gleis] = zug;
			this.notify();
		} else {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * F�hrt ein Zug aus ein Gleis raus falls ein im Gleis sich ein Zug befindet
	 * 
	 * @param gleis
	 * @return der ausgefahrene Zug
	 */
	public synchronized Zug ausfahren(int gleis) {
		Zug ausfahrenderZug = null;

		if (gleise[gleis] != null) {
			ausfahrenderZug = gleise[gleis];
			gleise[gleis] = null;
			this.notify();
		} else {
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return ausfahrenderZug;
	}

	/**
	 * getter
	 * 
	 * @return
	 */
	public Zug[] getGleise() {
		return gleise;
	}
}
