/**
 * Praktikum TIPR2 ,WS 2015
 * Gruppe: Soufian Ben afia
 * 		   Felix Naumann
 * Aufgabenblatt 3, Aufgabe 3
 */

package Aufgabenblatt3.Simulation;

import java.util.Observable;

import Aufgabenblatt3.Lokf�hrer.Lokf�hrer;
import Aufgabenblatt3.Lokf�hrer.Lokf�hrer.Aufgabe;
import Aufgabenblatt3.Rangierbahnhof.Rangierbahnhof;

/**
 * Simulation f�r das Ein- und Ausfahren von Z�gen aus einem Rangierbahnhof.
 * erzeugt jede 500 ms neue Lokf�hrer mit je einer Aufgabe. Der Lokf�hrer f�hrt
 * dann je nach Aufgabe entweder einen neuen Zug in den Bahnhof einoder aus dem
 * Bahnhof aus.
 */
public class Simulation extends Observable implements Runnable {

	private Rangierbahnhof rangierbahnhof = new Rangierbahnhof();
	private int gleis;
	boolean ausEin;

	@Override
	public void run() {

		ausEin = false;

		while (true) {
			try {
				Thread.sleep(500); // Jede 500 ms erzeugt ein neuen Lokf�hrer
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (ausEin) {
				gleis = (int) (Math.random() * 3);
				Lokf�hrer lokf�hrer = new Lokf�hrer(Aufgabe.AUSFAHREN, gleis, rangierbahnhof);
				lokf�hrer.start();
				ausEin = false;
				setChanged();
				notifyObservers();
			} else {
				gleis = (int) (Math.random() * 3);
				Lokf�hrer lokf�hrer = new Lokf�hrer(Aufgabe.EINFAHREN, gleis, rangierbahnhof);
				lokf�hrer.start();
				ausEin = true;
				setChanged();
				notifyObservers();
			}
		}
	}

	public boolean isAusEin() {
		return ausEin;
	}

	public int getGleis() {
		return gleis;
	}
}