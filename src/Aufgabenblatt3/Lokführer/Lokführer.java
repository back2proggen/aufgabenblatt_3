/**
 * Praktikum TIPR2 ,WS 2015
 * Gruppe: Soufian Ben afia
 * 		   Felix Naumann
 * Aufgabenblatt 3, Aufgabe 2
 */
package Aufgabenblatt3.Lokf�hrer;

import Aufgabenblatt3.Rangierbahnhof.Rangierbahnhof;
import Aufgabenblatt3.Rangierbahnhof.Zug;


/**
 * Lokf�hrer f�hrt ein Zug in ein Gleis rein oder raus.
 *
 */

public class Lokf�hrer extends Thread {

	/**
	 * aufgabe ist entweder Einfahren oder Ausfahren eines Zuges
	 */
	private Aufgabe aufgabe;
	/**
	 *  Die Gleisnummer
	 */
	private int gleis;
	
	/**
	 * Der Rangierbahnhof wo sich die Gleise befinden
	 */
	private Rangierbahnhof rangierbahnhof;

	
	/**
	 * Konstruktor 
	 * 
	 * @param aufgabe
	 * @param gleis
	 * @param rangierbahnhof
	 */
	public Lokf�hrer(Aufgabe aufgabe, int gleis, Rangierbahnhof rangierbahnhof) {
		this.aufgabe = aufgabe;
		this.gleis = gleis;
		this.rangierbahnhof = rangierbahnhof;
	}

	/**  
	 * Die Aufgaben des Lokf�hrers
	 */
	public enum Aufgabe {
		EINFAHREN, AUSFAHREN
	}

	@Override
	public void run() {

		switch (aufgabe) {
		case AUSFAHREN:
			rangierbahnhof.ausfahren(gleis);
			System.out.println("Ausfahren erfolgreich auf Gleis " + gleis);
			break;
		case EINFAHREN:
			rangierbahnhof.einfahren(gleis, new Zug());
			System.out.println("Einfahren erfolgreich auf Gleis " + gleis);
			break;
		}

	}
}
