package Aufgabenblatt3.Visualisierung;

import java.util.Observable;
import java.util.Observer;

import Aufgabenblatt3.Simulation.Simulation;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class Visualisierung extends Application implements Observer {

	static Simulation sim;
	Line line1 = new Line();
	Line line2 = new Line();
	Line line3 = new Line();

	public Visualisierung() {
		sim.addObserver(this);

	}

	public static void main(String[] args) {

		sim = new Simulation();
		new Thread(sim).start();
		launch();

	}

	public void start(Stage primaryStage) throws Exception {
		StackPane flowPane = new StackPane();
		zeichneGleise(flowPane);
		flowPane.getChildren().add(line1);
		flowPane.getChildren().add(line2);
		flowPane.getChildren().add(line3);

		primaryStage.setScene(new Scene(flowPane, 350, 300));
		primaryStage.show();
	}

	private void zeichneZug(Line line,int positionY) {
		line.setEndX(50);
		line.setStrokeWidth(30);
		line.setTranslateY(positionY);
	}
	
	private void loescheZug(Line line,int positionY) {
		line.setEndX(0);
		line.setStrokeWidth(0);
		line.setTranslateY(positionY);
	}
	

	private void zeichneGleise(Pane parentPane) {
		Line line = new Line();
		Line line1 = new Line();
		Line line2 = new Line();
		Line line3 = new Line();
		Line horizonzal = new Line();

		line.setEndX(100);
		line.setStrokeWidth(3);
		line.setTranslateY(-50);

		line1.setEndX(100);
		line1.setStrokeWidth(3);
		line1.setTranslateY(-100);

		line2.setEndX(100);
		line2.setEndY(0);
		line2.setStrokeWidth(3);

		line3.setEndX(100);
		line3.setStrokeWidth(3);
		line3.setTranslateY(50);

		horizonzal.setEndY(150);
		horizonzal.setStrokeWidth(3);
		horizonzal.setTranslateX(-50);
		horizonzal.setTranslateY(-25);
		parentPane.getChildren().addAll(line, line1, line2, line3, horizonzal);
	}

	@Override
	public void update(Observable arg0, Object arg1) {

		if (!sim.isAusEin()) {
			
			switch (sim.getGleis()) {
			case 0:
				zeichneZug(line1, -75);
				break;
			case 1:
				zeichneZug(line2, -25);
				break;
			case 2:
				zeichneZug(line3, 20);
				break;
			}
		}
		
		else
		{
			switch (sim.getGleis()) {
			case 0:
				loescheZug(line1, -75);
				break;
			case 1:
				loescheZug(line2, -25);
				break;
			case 2:
				loescheZug(line3, 20);
				break;
			}
		}

	}

}
